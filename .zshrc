# Yuki's zshrc
# ------------

export ZPLUG_HOME="$HOME/.zplug"

if ! type git > /dev/null; then
    echo "Git is not installed. Please install git."
    return
fi 

if [ ! -d "$ZPLUG_HOME" ]; then
    echo "Installing zplug ..."
    git clone https://github.com/zplug/zplug "$ZPLUG_HOME"
fi

# Aliases
# -------

alias dot="git --git-dir=$HOME/.cfg --work-tree=$HOME"

# Hacks
# -----

export GPG_TTY="$(tty)"

# Zsh configuration
# -----------------

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd beep extendedglob notify nomatch

bindkey -e

bindkey "^[Od" backward-word
bindkey "^[Oc" forward-word
bindkey "^[[3~" delete-char
bindkey "^H" backward-kill-word
bindkey "^[[A" history-substring-search-up
bindkey "^[[B" history-substring-search-down

# Portable(tm) Colored ls
# -----------------------

OS=$(uname -s)

case "$OS" in
    "OpenBSD")
        if command -v colorls >/dev/null 2>&1; then
            alias ls="colorls -G"
        fi
        ;;
    "FreeBSD")
        alias ls="ls -G"
        ;;
    "DragonFly")
        alias ls="ls -G"
        ;;
    "Linux")
        alias ls="ls --color"
        ;;
    "SunOS")
        # TODO: Proper checks to see if it's GNU
        alias ls="ls --color"
        ;;
    *)
        # Boo, no colored ls :(
        ;;
esac

# zplug configuration
# -------------------

source "$ZPLUG_HOME/init.zsh"

zplug "plugins/python", from:oh-my-zsh
zplug "plugins/pip", from:oh-my-zsh
zplug "plugins/virtualenvwrapper", from:oh-my-zsh

zplug "zsh-users/zsh-history-substring-search"
zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-syntax-highlighting"
zplug "zsh-users/zsh-completions"


if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
	echo; zplug install
    fi
fi

zplug load 
